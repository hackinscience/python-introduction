<!-- .slide: data-background="./devs.png" -->

----


# Python 3.7
<tt>in ~37 minutes</tt>

Notes: Introduce yourself!

----

# Python: Introduction

Python is a programming language<br/>
giving you the opportunity to express yourself in a **concise** and **readable** way.

----

# Python 2 / Python 3

On windows:

```
py
```

On other systems:

```
python3
```

Notes: `py` on Windows starts the latest installed version.


----

# Who uses Python

- YouTube, Dropbox, Reddit, Instagram, Spotify, ...
- Data scientists
- IA, Big Data, Deep Learning
- Check the TIOBE index :)

----

# Installing

Use your favorite editor, or try PyCharm and VS Code.

Install from https://python.org.

Notes:
- On windows use the WSL, or gitforwindows.org if you can't
- https://docs.python.org/3/using/windows.html
- https://docs.python.org/3/using/mac.html
- On windows, don't install from the Microsoft Store.

----

# Read Eval Print Loop

The *REPL*, also called *the interpreter*, or *the interactive console*, this is
the place where we try things.

Notes:

And yes, we still use it after 10 years of writing Python.

----

# Read Eval Print Loop

You can get a REPL by starting Python, IDLE, IPython, or whatever, as long as you see:

```bash
>>>
```
or
```bash
In [1]:
```

It's OK.

----

# Read Eval Print Loop

```python
>>> print("Hello world")
Hello world
>>>
```

Notes:

Show them the:

- Read
- Eval
- Print
- Loop (Read again)

----

# Instructions + data = program

```python
>>> print("Hello world")
```

`print` is a built-in function, "Hello world" is data.

Notes:

Instructions may contains mathematical operators, or functions like `print` to show a result.

----

# Let's play with the REPL!

```python
>>> 10
10
>>>
```
Notes:

Python understood that it's a decimal integer which value is 10, and represent it like us as two characters, ‘1' followed by ‘0'

----

# The REPL is your new calculator

```python
>>> 5 + 18
23
>>>
```

Notes:
Let's give him an addition.

Python understood the statement and showed us the result

----

# Exceptions
## Read the last line first

```python
>>> 5 -* 3
  File "<stdin>", line 1
    5 -*
       ^
SyntaxError: invalid syntax
```

Notes:
Let's give him some nonsense...
Python is explicit when something's wrong.
SyntaxError ...
----

# Booleans

```python
>>> True
True
>>> False
False
```

----

# Booleans operators

```python
>>> True or False
True
>>> True and False
False
```
----

# Lists

```python
>>> [1, 2, 3, 4, 5, 6, 7, 8]
[1, 2, 3, 4, 5, 6, 7, 8]
```

Notes:

As a human, use "[" and "]" to write lists to Python:

Python uses the same syntax to show a list to a human.

----

# Mixing types is allowed

```python
>>> [1, 0x2A, 5j, 5.333]
[1, 42, 5j, 5.333]
```

Notes:
Mixing data of different types may fail in a lot of languages…
But it's OK in Python!

----

# Even lists in lists is allowed

```python
>>> [1, 2, [1, 2, 3], 3]
[1, 2, [1, 2, 3], 3]
```
Notes:
Lists are data, data can be mixed:
We'll have to try harder to break Python

----

# Strings

```python
>>> 'La vie de Brian'
'La vie de Brian'
>>> "Le Sens de la vie"
'Le Sens de la vie'
>>> """La vie de Brian"""
'La vie de Brian'
```
Notes:
A string using quotes
Or triple quotes !
... double quotes
Take the time needed to explain what a string is… but don't speak about chars/pointers, we're not in a C89 class.

----

# Tuples
```python
>>> 1, 2, 3
(1, 2, 3)
>>> ("Graham", "John", "Terry")
('Graham', 'John', 'Terry')
```
Notes:
Using commas glues data as tuples.
Any type of data….

----

# Sets
```python
>>> {1, 2, 1, 3, 1, 4, 5}
{1, 2, 3, 4, 5}
```
Notes:
A set is a collection which can't contain duplicates and is not ordered.

----

# Dictionaries

```python
>>> {"Graham": 1941, "John": 1939}
{"Graham": 1941, "John": 1939}
```
Notes:
A dictionary is the association of keys and values.

----

# Operators

```python
>>> 10 + 10
20
>>> 1j * 1j
(-1+0j)
>>> 10.5 + 2
12.5
```

----

# Comparisons

```python
>>> 10 < 1
False
>>> 10 == 10
True
>>> 10 == 20
False
```

----

# Operators on other types


```python
>>> "La vie " + "de Brian"
'La vie de Brian'
```
Notes:
It's called concatenation of strings.

----

# Operators on mixed types

```python
>>> "Tu Tum Pak " * 5
'Tu Tum Pak Tu Tum Pak Tu Tum Pak Tu Tum Pak Tu Tum Pak '
```
Notes:
As long as it make sense, Python tries to do it.

----

# Containment testing

```python
>>> "aa" in "sacré graal"
True
```
Notes:
Containment testing.

----

# Operators on other types

```python
>>> ["Graham", "John"] + [1, 2, 3]
['Graham', 'John', 1, 2, 3]
```

Notes:

As long as it make sense and is obvious: it will work.

If it's ambiguous, it will not be implemented.

----

# Working with sets

```python
>>> {"a", "b"} | {"a", "x", "y"}
{'b', 'y', 'a', 'x'}
```

Notes:
Union of two sets.

----

# Working with sets

```python
>>> {"a", "b"} & {"a", "x", "y"}
{'a'}
```

Notes:
Intersection...

----

# Working with sets

```python
>>> "b" in {"a", "b", "c"}
True
```

----

# When it does not make sense...

```python
>>> "Doh" * "Dah"
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: can't multiply sequence by non-int of type 'str'
```

----

# Or when it's ambiguous...

```python
>>> {"a", "b"} + {"a", "x", "y"}
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
TypeError: unsupported operand type(s) for +: 'set' and 'set'
```

----

# Methods

```python
>>> "La vie de Brian".upper()
'LA VIE DE BRIAN'
>>> "La vie de Brian".isupper()
False
>>> "La vie de Brian".startswith("La")
True
>>> "La vie de Brian".startswith("Vie")
False
```

Notes:
Data may have methods, depending on its type.
```

----

# Variables

```python
>>> names = ["Graham", "John", "Terry"]
>>> names
["Graham", "John", "Terry"]
```
Notes:
Variables are just names, we use variables to name data and reuse it.
----

# Accessing items

```python
>>> names[0]
'Graham'
>>> names[1]
'John'
```
Notes:
We're reusing the names variable to extract items.
Take the needed time to explain the list[n] syntax.

----

# Accessing items

```python
>>> ["Graham", "John"][0]
'Graham'
```

Notes:
Yes, it work, why not? It's like eating directly in the saucepan.

----

# Accessing items of a dict

```python
>>> births = {"Graham": 1941,
...           "John": 1939}
>>> births["John"]
1939
```

----

# Control flow

```python
>>> if names.count("Terry") > 1:
...     print("There's two Terry")
... else:
...     print("Only one Terry")
...
There's two Terry
```

Notes:
Control flow: "if, else".

Note the "dot dot dot", it means Python expect you to continue typing
lines. Just type enter on an empty one to break out of this.

----

# Functions

```python
>>> def max(a, b):
...     if a > b:
...         return a
...     else:
...         return b
...
>>> max(10, 20)
20
```
Notes:
Builtins functions are cool, defining ours is WOW.

----

# The documentation

- https://docs.python.org/3/library/
  - Built-in Types
  - Built-in Functions
- https://docs.python.org/3/tutorial/
